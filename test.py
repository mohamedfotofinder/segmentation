import os
import random
import h5py

import cv2
import skimage
print(skimage.__version__)
import tensorflow as tf
import mrcnn.model as modellib
from mrcnn import utils
from mrcnn import visualize
from mrcnn.visualize import display_images
from mrcnn.model import log

import warnings
from BodyDataset import BodyDataset
from BodyConfig import BodyConfig

warnings.filterwarnings('ignore')
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
tf.logging.set_verbosity(tf.logging.ERROR)


ROOT_DIR = os.getcwd()
MASK_DIR = os.path.abspath("./Mask_RCNN")
# print(ROOT_DIR)
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

dataset_train = BodyDataset()
COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")

dataset_path = os.path.join(ROOT_DIR,"data20")
dataset_test_path = os.path.join(dataset_path, "val")
config = BodyConfig()


################
class InferenceConfig(config.__class__):
    # Run detection on one image at a time
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    KEYPOINT_MASK_POOL_SIZE = 17
    #IMAGE_MIN_DIM = 512
    #IMAGE_MAX_DIM = 512
    #DETECTION_MIN_CONFIDENCE = 0.70

config = InferenceConfig()
config.display()
# Device to load the neural network on.
# Useful if you're training a model on the same
# machine, in which case use CPU and leave the
# GPU for training.
DEVICE = "/cpu:0"  # /cpu:0 or /gpu:0

TEST_MODE = "inference"
# Load validation dataset
dataset_test = BodyDataset()
dataset_test.load_body(dataset_test_path, "val")
dataset_test.prepare()

# Create model in inference mode


# Download file from the Releases page and set its path
# https://github.com/matterport/Mask_RCNN/releases
# weights_path = "/path/to/mask_rcnn_idv.h5"

# Or, load the last model you trained
#weights_path = 'C:/Users/m_chrigui/Desktop/fotofinder/SegmentationSkin/mask_rcnn_skin_0016.h5'
#weights_path =r'C:\Users\m_chrigui\Desktop\fotofinder\SegmentationSkin\logs\body20190820T1358\mask_rcnn_body_0001.h5'
weights_path = 'mask_rcnn_coco_humanpose.h5'

#a_h5 = h5py.File(weights_path)
#a_h5.values()

# Load weights
#print("Loading weights ", weights_path)
#model.load_weights(weights_path, by_name=True)
#num = 'FotoFinderUniverse_0309181614_FFI_093315.jpg'

model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)
# Load weights trained on MS-COCO
model.load_weights(weights_path, by_name=True)
"""model.load_weights(weights_path, by_name=True, exclude=[
            "mrcnn_class_logits", "mrcnn_bbox_fc",
            "mrcnn_bbox", "mrcnn_mask"])"""

#model.train(dataset_train, dataset_val, learning_rate=config.LEARNING_RATE, epochs=1, layers='heads')

class_names = ['BG', 'person']
"""class_names = ['BG', 'body', 'bicycle', 'car', 'motorcycle', 'airplane',
                   'bus', 'train', 'truck', 'boat', 'traffic light',
                   'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
                   'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
                   'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
                   'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
                   'kite', 'baseball bat', 'baseball glove', 'skateboard',
                   'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
                   'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
                   'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
                   'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
                   'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
                   'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
                   'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
                   'teddy bear', 'hair drier', 'toothbrush']
"""
# Load a random image from the images folder
file_names = next(os.walk(dataset_test_path))[2]
image = skimage.io.imread(os.path.join(dataset_test_path, random.choice(file_names)))

# Run detection
results = model.detect([image], verbose=1)
# Visualize results
r = results[0]

visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'], class_names, r['scores'], figsize=(16,16))
#visualize.display_instances(image, r['masks'],)