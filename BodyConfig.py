from mrcnn.config import Config
class BodyConfig(Config):
    NAME = 'body'
    GPU_COUNT = 1
    IMAGES_PER_GPU = 2
    NUM_CLASSES = 1 + 1 #one for body and the other for background
    #IMAGE_MIN_DIM = 640
    #IMAGE_MAX_DIM = 4096
    #RPN_ANCHOR_SCALES = (16 * 16, 32 * 128, 64 * 128, 32 * 256, 64 * 512)
    #TRAIN_ROIS_PER_IMAGE = 32
    STEPS_PER_EPOCH = 50
    VALIDATION_STEPS = 20
    DETECTION_MIN_CONFIDENCE = 0.8
    #LEARNING_RATE = 0.01
    LEARNING_RATE = 0.01
